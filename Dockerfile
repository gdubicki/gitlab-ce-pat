ARG BASE_EDITION=ee
ARG BASE_VERSION=latest

FROM gitlab/gitlab-${BASE_EDITION}:${BASE_VERSION}

ARG GITLAB_TOKEN="glpat-ACCTEST1234567890123"
ENV GITLAB_TOKEN=$GITLAB_TOKEN

RUN mkdir -p /usr/local/bin
ADD healthcheck-and-setup.sh /usr/local/bin/healthcheck-and-setup.sh

HEALTHCHECK --interval=60s --timeout=30s --retries=1 CMD /usr/local/bin/healthcheck-and-setup.sh
