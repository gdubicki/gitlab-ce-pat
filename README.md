This image can be used to run GitLab in a container with the REST API accessible using an admin user Personal Access Token for full access.

It can be useful f.e. to run acceptance test against a real GitLab instance in a container.

# Usage

The image is available in DockerHub under:

* CE - [gdubicki/gitlab-ce-pat](https://hub.docker.com/r/gdubicki/gitlab-ce-pat),
* EE - [gdubicki/gitlab-ee-pat](https://hub.docker.com/r/gdubicki/gitlab-ee-pat),

...and can be used f.e. like this:

```shell
docker run -p 443:443 -p 80:80 -p 2022:22 --name gitlab-pat gdubicki/gitlab-ee-pat:latest
```

The Token that can be used for REST API requests is `glpat-ACCTEST1234567890123`. Example REST API request to test:
```shell
curl -s -H "Authorization:Bearer glpat-ACCTEST1234567890123 http://localhost/api/v4/version
```

(The Token can be reconfigured by rebuilding the image with a custom `GITLAB_TOKEN` argument).

# License

The [healthcheck-and-setup.sh](./healthcheck-and-setup.sh) script is based on [a script from the GitLab Terraform Provider](https://gitlab.com/gitlab-org/terraform-provider-gitlab/-/blob/main/scripts/healthcheck-and-setup.sh?ref_type=heads) licensed under the MPL 2.0 license, so to make it simple the whole contents of this repo is licensed under the same license.

# Disclaimer

This image is unofficial and is not supported or endorsed by GitLab Inc.
