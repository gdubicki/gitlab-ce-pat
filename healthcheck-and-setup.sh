#!/usr/bin/env sh

# This script is intended to be used as a Docker HEALTHCHECK for the GitLab container.
# It prepares GitLab prior to running acceptance tests.
#
# This is a known workaround for docker-compose lacking lifecycle hooks.
# See: https://github.com/docker/compose/issues/1809#issuecomment-657815188

set -e

# Check for a successful HTTP status code from GitLab.
curl --silent --show-error --fail --output /dev/null 127.0.0.1:80

# Because this script runs on a regular health check interval,
# this file functions as a marker that tells us if initialization already finished.
done=/var/gitlab-acctest-initialized

test -f $done || {
  echo 'Initializing GitLab for REST API requests'

  echo 'Creating access token'
  (
    printf 'terraform_token = PersonalAccessToken.create('
    printf 'user_id: 1, '
    printf 'scopes: [:api, :read_user], '
    printf 'name: :root, '
    printf 'expires_at: Time.now + 30.days);'
    printf "terraform_token.set_token('$GITLAB_TOKEN');"
    printf 'terraform_token.save!;'
  ) | gitlab-rails console

  touch $done
}

echo 'GitLab is ready for REST API requests'
